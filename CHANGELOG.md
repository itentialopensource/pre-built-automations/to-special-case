
## 0.0.8 [02-04-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/to-special-case!4

---

## 0.0.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/to-special-case!3

---

## 0.0.6 [06-02-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/to-special-case!2

---

## 0.0.5 [12-03-2021]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/to-special-case!1

---

## 0.0.4 [11-17-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.3 [11-17-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [11-17-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
