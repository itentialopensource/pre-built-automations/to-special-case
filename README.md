<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

<!-- Update the below line with your Pre-Built name -->
# To Special Case

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Examples](#examples)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
This Pre-Built consists of a JST that takes in words separated by whitespace and converts them to camelCase, PascalCase, kebab-case, and snake_case. 

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1.x`


## Future Enhancements

<!-- OPTIONAL - Mention if the Pre-Built will be enhanced with additional features on the road map -->
<!-- Ex.: This Pre-Built would support Cisco XR and F5 devices -->
* Converting words with a non-whitespace seperator into alternate cases

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

In order to run this JST, add the appropriate JST task onto the workflow, then search for "toSpecialCase" and
assign values to the incoming variable, incomingString. 


<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Examples

***Input***

```
{
  "incomingString": "foo bar foobar"
}
```

***Output***
```
{
  "toCamelCase": "fooBarFoobar",
  "toSnakeCase": "foo_bar_foobar",
  "toKebabCase": "foo-bar-foobar",
  "toPascalCase": "FooBarFoobar"
}
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
